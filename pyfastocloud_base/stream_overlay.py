from pyfastogt.maker import Maker


class StreamOverlay(Maker):
    URL_FIELD = "url"
    BACKGROUND_FIELD = "background"
    ALPHA_FIELD = "alpha"
    SIZE_FIELD = "size"

    def __init__(self):
        self.url = str()
        self.background = None
        self.alpha = None
        self.size = None

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, url = self.check_required_type(StreamOverlay.URL_FIELD, str, json)
        if res:
            self.url = url

        res, back = self.check_optional_type(StreamOverlay.BACKGROUND_FIELD, int, json)
        if res:
            self.background = back
        else:
            self.background = None

        res, alpha = self.check_optional_type(StreamOverlay.ALPHA_FIELD, float, json)
        if res:
            self.alpha = alpha
        else:
            self.alpha = None

        res, size = self.check_optional_type(StreamOverlay.SIZE_FIELD, dict, json)
        if res:
            self.size = size
        else:
            self.size = None

    def to_dict(self) -> dict:
        result = {StreamOverlay.URL_FIELD: self.url}
        if self.background is not None:
            result[StreamOverlay.BACKGROUND_FIELD] = self.background
        if self.alpha is not None:
            result[StreamOverlay.ALPHA_FIELD] = self.alpha
        if self.size is not None:
            result[StreamOverlay.SIZE_FIELD] = self.size

        return result
